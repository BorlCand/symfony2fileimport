<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170310095729 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $schema->getTable('tblProductData')->addColumn('dcmCost', 'decimal', ['precision' => 19, 'scale' => 4]);
        $schema->getTable('tblProductData')->addColumn('intStock', 'integer');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $schema->getTable('tblProductData')->dropColumn('dcmCost');
        $schema->getTable('tblProductData')->dropColumn('intStock');
    }
}
