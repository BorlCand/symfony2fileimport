<?php

namespace AppBundle\Command;

use AppBundle\Port\Steps\UpdateDatetimeFieldsStep;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class ImportCsvCommand extends ContainerAwareCommand
{
    private $header = array('productCode', 'productName', 'productDesc', 'stock', 'cost', 'discontinued');
    private $entity = 'AppBundle\Entity\ProductData';
    private $index = 'productCode';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:import-csv')
            ->setDescription('Imports file, defined as parameter')
            ->setHelp("Allows you to import large datasets from CSV files to your database")
            ->addArgument('file', InputArgument::REQUIRED, 'Path to the importing file')
            ->addOption('delimiter', 'd', InputOption::VALUE_OPTIONAL, 'Values separator', ',')
            ->addOption('test', 't', InputOption::VALUE_NONE, 'Perform test')
            ->addOption('update', 'u', InputOption::VALUE_NONE, 'Allow to update existing entities')
            ->addOption('truncate', null, InputOption::VALUE_NONE, 'Truncate table before import')
            ->addOption('batch', 'b', InputOption::VALUE_OPTIONAL, 'Max chunk size', 20)
            ->addOption('entity', null, InputOption::VALUE_OPTIONAL, 'Entity, which will validate and write entries.', $this->entity)
            ->addOption('index', null, InputOption::VALUE_OPTIONAL, 'Index, which will be used to filter duplicate entities.', $this->index)
            ->addOption('header', null, InputOption::VALUE_IS_ARRAY | InputOption::VALUE_OPTIONAL, 'Headers used to write items to database. Must match entity properties.', $this->header);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fileName = realpath($input->getArgument('file'));
        if (is_readable($fileName) && !is_dir($fileName)) {
            $file = new \SplFileObject($fileName);
        } else {
            throw new FileNotFoundException($path = $fileName);
        }

        $output->writeln([
            '<question>                                 </question>',
            '<question>          File importer          </question>',
            '<question>=================================</question>',
            '',
            '<comment>File to be imported: </comment><info>'. basename($file->getBasename()) . '</info>',
            '<comment>Path to file: </comment><info>' . $fileName . '</info>'
        ]);

        $result = $this->getContainer()->get("fileimport.csv")
            ->initialize($input->getOptions())
            ->addSteps(new UpdateDatetimeFieldsStep())
            ->import($file);

        $output->writeln("\n<comment>Elapsed time: </comment><info>".$result->getElapsed()->format("%H:%I:%S"). "</info>");
        $output->writeln("<comment>Succeed: </comment><info>".$result->getSuccessCount()."</info>");
        $output->writeln("<comment>Error: </comment>". ($result->getErrorCount() > 0 ? "<error>" . $result->getErrorCount() . "</error>" : "<info>0</info>"));
        if ($result->getExceptions()->count() > 0){
            $output->writeln("<error>Exceptions:</error>\n");

            foreach ($result->getExceptions() as $e){
                $output->writeln([$e->getMessage(), ""]);
            }
        }
    }
}
