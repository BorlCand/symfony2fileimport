<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Puhovsky\ValidatorsBundle\Validator\Constraints as PAssert;

/**
 * ProductData
 *
 * @ORM\Table(name="tblProductData", uniqueConstraints={@ORM\UniqueConstraint(name="strProductCode", columns={"strProductCode"})})
 * @ORM\Entity
 */
class ProductData
{
    use TimestampableEntity;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max = 50)
     * @ORM\Column(name="strProductName", type="string", length=50, nullable=false)
     */
    private $productName;

    /**
     * @var string
     *
     * @Assert\Collection\Optional()
     * @Assert\Length(max = 255)
     * @ORM\Column(name="strProductDesc", type="string", length=255, nullable=false)
     */
    private $productDesc;

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max = 10)
     * @ORM\Column(name="strProductCode", type="string", length=10, nullable=false, unique=true)
     */
    private $productCode;

    /**
     * @var string
     *
     * @Assert\Collection\Optional()
     * @Assert\DateTime()
     * @ORM\Column(name="dtmAdded", type="datetime", nullable=true)
     */
    private $added;

    /**
     * @var string
     *
     * @Assert\Collection\Optional()
     * @Assert\DateTime(message = "Value should be either 'yes' or empty")
     * @ORM\Column(name="dtmDiscontinued", type="datetime", nullable=true)
     */
    private $discontinued;

    /**
     * @var string
     *
     * @ORM\Column(name="stmTimestamp", type="datetime", nullable=false)
     */
    private $updated;

    /**
     * @var double
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="Only positive scalars between 0 and 1000 are allowed"
     * )
     * @Assert\GreaterThanOrEqual(0.00)
     * @Assert\LessThanOrEqual(1000.00)
     * @Assert\Expression(
     *     "value >= 5 or this.getStock() >= 10",
     *     message="If item costs less than GPB 5 it should have more than 10 items in stock!"
     * )
     * @ORM\Column(name="dcmCost", type="decimal", precision=19, scale=4, nullable=false)
     */
    private $cost;

    /**
     * @var integer
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     * @PAssert\StringInteger()
     * @ORM\Column(name="intStock", type="integer", nullable=false)
     */
    private $stock;

    /**
     * @var integer
     *
     * @ORM\Column(name="intProductDataId", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $productId;

    public function __construct($item = [])
    {
        foreach ($item as $index => $value) {
            if (property_exists($this, $index)){
                $this->{$index} = $value;
            }
        }
    }

    /**
     * Set productName
     *
     * @param string $productName
     *
     * @return ProductData
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set productDesc
     *
     * @param string $productDesc
     *
     * @return ProductData
     */
    public function setProductDesc($productDesc)
    {
        $this->productDesc = $productDesc;

        return $this;
    }

    /**
     * Get productDesc
     *
     * @return string
     */
    public function getProductDesc()
    {
        return $this->productDesc;
    }

    /**
     * Set productCode
     *
     * @param string $productCode
     *
     * @return ProductData
     */
    public function setProductCode($productCode)
    {
        $this->productCode = $productCode;

        return $this;
    }

    /**
     * Get productCode
     *
     * @return string
     */
    public function getProductCode()
    {
        return $this->productCode;
    }

    /**
     * Set added
     *
     * @param \DateTime $added
     *
     * @return ProductData
     */
    public function setAdded($added)
    {
        if (is_null($this->added)){
            $this->added = $added;
        }

        return $this;
    }

    /**
     * Get added
     *
     * @return \DateTime
     */
    public function getAdded()
    {
        return $this->added;
    }

    /**
     * Set discontinued
     *
     * @param \DateTime $discontinued
     *
     * @return ProductData
     */
    public function setDiscontinued($discontinued)
    {
        $this->discontinued = $discontinued;

        return $this;
    }

    /**
     * Get discontinued
     *
     * @return \DateTime
     */
    public function getDiscontinued()
    {
        return $this->discontinued;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     *
     * @return ProductData
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set cost
     *
     * @param double $cost
     *
     * @return ProductData
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return double
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set stock
     *
     * @param integer $stock
     *
     * @return ProductData
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * Get stock
     *
     * @return integer
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * Get productId
     *
     * @return integer
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
