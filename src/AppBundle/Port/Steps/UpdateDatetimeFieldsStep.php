<?php

namespace AppBundle\Port\Steps;

use Port\Steps\Step;

class UpdateDatetimeFieldsStep implements Step
{
    public function process($item, callable $next)
    {
        $now = new \DateTime("now");
        $item['added'] = $now;

        if ($item['discontinued'] == 'yes'){
            $item['discontinued'] = $now;
        } elseif (empty($item['discontinued'])) {
            $item['discontinued'] = null;
        }


        return $next($item);
    }
}