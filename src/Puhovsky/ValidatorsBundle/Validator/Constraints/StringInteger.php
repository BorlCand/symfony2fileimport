<?php

namespace Puhovsky\ValidatorsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class StringInteger extends Constraint
{
    public $message = 'The string "%string%" contains a non-integer character';
}