<?php

namespace Puhovsky\ValidatorsBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class StringIntegerValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ((string)(int)$value != $value) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}