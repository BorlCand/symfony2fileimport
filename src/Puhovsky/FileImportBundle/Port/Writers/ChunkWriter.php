<?php

namespace Puhovsky\FileImportBundle\Port\Writers;

use Port\Exception\UnexpectedValueException;
use Port\Writer;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ChunkWriter implements Writer
{
    /**
     * @var Writer
     */
    protected $delegate;

    /**
     * @var integer
     */
    protected $size;

    /**
     * @var \SplQueue
     */
    protected $queue;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * @param Writer $delegate
     * @param integer $size
     */
    public function __construct(Writer $delegate, ValidatorInterface $validator, $size = 20)
    {
        $this->delegate = $delegate;
        $this->size = $size;
    }

    /**
     * {@inheritdoc}
     */
    public function prepare()
    {
        $this->delegate->prepare();
        $this->queue = new \SplQueue();
    }

    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        $validationResult = $this->validator->validate($object);

        if (empty($validationResult->count())) {
            $this->queue->push($item);
            if (count($this->queue) >= $this->size) {
                $this->flush();
            }
        } else {
            foreach ($validationResult as $violation) {
                throw new UnexpectedValueException($violation);
            }

        }
    }

    /**
     * {@inheritdoc}
     */
    public function finish()
    {
        $this->flush();
        $this->delegate->finish();
    }

    /**
     * Flush the internal buffer to the delegated writer
     */
    private function flush()
    {
        foreach ($this->queue as $item) {
            $this->delegate->writeItem($item);
        }
        if ($this->delegate instanceof FlushableWriter) {
            $this->delegate->flush();
        }
    }
}