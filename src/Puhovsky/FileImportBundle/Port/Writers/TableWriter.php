<?php

namespace Puhovsky\FileImportBundle\Port\Writers;

use Port\SymfonyConsole\TableWriter as TWriter;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Port\Exception\UnexpectedValueException;

class TableWriter extends TWriter
{
    use QueueTemplate;

    protected $objectName;
    protected $validator;

    /**
     * {@inheritdoc}
     */
    protected $table;

    /**
     * {@inheritdoc}
     */
    protected $firstItem;

    /**
     * Field used to lookup an object
     *
     * @var string
     */
    protected $lookupField = null;

    /**
     * Object indexes to filter duplicates
     *
     * @var array
     */
    protected $objectIndexes = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(ValidatorInterface $validator, Table $table, $objectName, $index = null)
    {
        $this->objectName = $objectName;
        $this->validator = $validator;
        $this->table = $table;
        $this->lookupField = $index;

        parent::__construct($table); //ToDo: Since we cannot use parent properties we can switch to implement Port\Writer
    }

    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        $this->validateItem($item);

        $item = $this->convertDateTimeToString($item);
        // Save first item to get keys to display at header
        if (is_null($this->firstItem)) {
            $this->firstItem = $item;
        }

        $this->table->addRow($item);
        $this->objectCount++;

        if ($this->lookupField){
            $this->objectIndexes[] = $item[$this->lookupField];
        }

        if ($this->queueSize != 0 && $this->objectCount >= $this->queueSize){
            $this->flush();
        }

    }

    protected function validateItem($item){
        $object = new $this->objectName($item);
        $errors = $this->validator->validate($object);
        $object = null;

        if ($this->lookupField && in_array($item[$this->lookupField], $this->objectIndexes)) {
            throw new UnexpectedValueException(
                sprintf(
                    "<question>=> Item with such unique id already exists:</question> <options=bold>%s</>\n<info>%s</info>",
                    $item[$this->lookupField],
                    json_encode($item)
                )
            );
        }

        if ($errors->count()){
            foreach ($errors as $error) {
                throw new UnexpectedValueException(
                    sprintf(
                        "<error>=> Item contains errors:</error>\n<info>%s</info>\n<comment>Field triggered an error:</comment> <options=bold>%s</>\n<comment>Field value:</comment> <options=bold>%s</>\n<comment>Error message:</comment> <info>%s</info>",
                        json_encode($item),
                        $error->getPropertyPath(),
                        $error->getInvalidValue(),
                        $error->getMessage()
                    )
                );
            }
        } else {
            return true;
        }
    }

    /**
     * Symfony Table does not support DateTime type. Reformatting it to string
     *
     * @param $item
     * @return mixed
     */
    protected function convertDateTimeToString($item){
        foreach ($item as $index => $value){
            if ($value instanceof \DateTime){
                $item[$index] = $item[$index]->format("Y-m-d H:i:s");
            }
        }

        return $item;
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $this->objectCount = 0;
        $this->table->setHeaders(array_keys($this->firstItem));
        $this->table->render();
        $this->table->setRows([]);
    }

    /**
     * {@inheritdoc}
     */
    public function finish() {
        if ($this->objectCount != 0) {
            $this->table->setHeaders(array_keys($this->firstItem));
            $this->table->render();
        }

        $this->firstItem = null;
    }
}