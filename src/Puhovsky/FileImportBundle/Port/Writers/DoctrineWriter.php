<?php

namespace Puhovsky\FileImportBundle\Port\Writers;

use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Port\Doctrine\DoctrineWriter as DWriter;
use Port\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class DoctrineWriter extends DWriter
{
    use QueueTemplate;

    /**
     * @var ValidatorInterface
     */
    protected $validator;

    /**
     * Whether to update the model if possible
     *
     * @var boolean
     */
    protected $update = true;

    /**
     * Object indexes to filter duplicates
     *
     * @var string[]
     */
    protected $objectIndexes = [];

    /**
     * {@inheritdoc}
     */
    public function __construct(ValidatorInterface $validator, ObjectManager $objectManager, $objectName, $index = null, $lookupMethod = 'findOneBy')
    {
        $this->validator = $validator;

        parent::__construct($objectManager, $objectName, $index, $lookupMethod);
    }

    /**
     * {@inheritdoc}
     */
    public function writeItem(array $item)
    {
        $object = $this->findOrCreateItem($item);

        $this->loadAssociationObjectsToObject($item, $object);

        $this->validateItem($item, $object);

        $this->objectManager->persist($object);
        $this->objectCount++;

        if (!empty($this->lookupFields)) {
            $this->objectIndexes[] = $item[$this->lookupFields[0]];
        }

        if ($this->queueSize != 0 && $this->objectCount >= $this->queueSize) {
            $this->flush();
        }
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $this->objectCount = 0;

        parent::flush();
    }

    /**
     * Get count of persisted objects
     *
     * @return int
     */
    public function getObjectCount()
    {
        return $this->objectCount;
    }

    /**
     * @return boolean
     */
    public function getUpdate()
    {
        return $this->update;
    }

    /**
     * Set whether to update the model if possible
     *
     * @param boolean $update
     *
     * @return $this
     */
    public function setUpdate($update)
    {
        $this->update = $update;

        return $this;
    }

    /**
     * Disable updating models, only create
     *
     * @return $this
     */
    public function disableUpdate()
    {
        $this->update = false;

        return $this;
    }

    /**
     * Validates item using entity and ValidationInterface
     *
     * @param $item
     * @param $object
     * @return bool
     */
    protected function validateItem($item, &$object)
    {
        if (($this->existsInObjectManager($object) || $this->existsInObjectsIndexes($item)) && !$this->update) {
            throw new UnexpectedValueException(
                sprintf(
                    "<question>=> Item with such unique id already exists:</question> <options=bold>%s</>\n<info>%s</info>",
                    $object->{'get' . Inflector::classify($this->lookupFields[0])}(),
                    json_encode($item)
                )
            );
        }

        $this->updateObject($item, $object);

        $errors = $this->validator->validate($object);

        if ($errors->count()) {
            foreach ($errors as $error) {
                throw new UnexpectedValueException(
                    sprintf(
                        "<error>=> Item contains errors:</error>\n<info>%s</info>\n<comment>Field triggered an error:</comment> <options=bold>%s</>\n<comment>Field value:</comment> <options=bold>%s</>\n<comment>Error message:</comment> <info>%s</info>",
                        json_encode($item),
                        $error->getPropertyPath(),
                        $error->getInvalidValue(),
                        $error->getMessage()
                    )
                );
            }
        } else {
            return true;
        }
    }

    protected function existsInObjectsIndexes($item)
    {
        return !empty($this->lookupFields) && in_array($item[$this->lookupFields[0]], $this->objectIndexes);
    }

    protected function existsInObjectManager($object)
    {
        return $object->{'get' . Inflector::classify($this->lookupFields[0])}() !== null;
    }
}