<?php

namespace Puhovsky\FileImportBundle\Port\Writers;

trait QueueTemplate
{

    /**
     * How many objects should be persisted to perform automatic flush
     * Set 0 to disable
     *
     * @var int
     */
    protected $queueSize = 1000;

    /**
     * Count of persisted objects
     *
     * @var int
     */
    protected $objectCount = 0;

    /**
     * @return int
     */
    public function getQueueSize()
    {
        return $this->queueSize;
    }

    /**
     * Set how many objects should be persisted to perform automatic flush
     * Set to 0 to disable
     *
     * @param int $queueSize
     *
     * @return $this
     */
    public function setQueueSize($queueSize)
    {
        $this->queueSize = $queueSize;

        return $this;
    }

    /**
     * Disable automatic flush
     *
     * @return $this
     */
    public function disableQueue()
    {
        $this->queueSize = 0;

        return $this;
    }

}