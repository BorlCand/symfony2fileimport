<?php

namespace Puhovsky\FileImportBundle\Port;

use Port\Result;

class EditableResult extends Result
{
    public function setErrorCount(int $count)
    {
        $this->errorCount += $count;
    }

    public function setTotalProcessedCount(int $count)
    {
        $this->totalProcessedCount += $count;
    }
}