<?php

namespace Puhovsky\FileImportBundle\Tests\Importers;

use Puhovsky\FileImportBundle\Importers\CsvImporter;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CsvImporterTest extends KernelTestCase
{
    /**
     * @var CsvImporter
     */
    private $object;

    /**
     * @var \Port\Result
     */
    private $result;

    /**
     * @var array
     */
    protected $arguments = [
        "test" => true,
        "update" => false,
        "truncate" => false,
        "batch" => 1000,
        "delimiter" => ',',
        "enclosure" => '"',
        "escape" => '\\',
        "header" => null,
        "entity" => 'Puhovsky\FileImportBundle\Entity\TestModel',
        "index" => 'stringUnique',
    ];

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->object = static::$kernel->getContainer()
            ->get('fileimport.csv');

        $this->result = $this->object
            ->initialize($this->arguments)
            ->import(
                new \SplFileObject(
                    static::$kernel->locateResource('@FileImportBundle/Resources/fixture/testModel.csv')
                )
            );
        
        #/echo "\nElapsed time: " . $this->result->getElapsed()->format("%H:%I:%S") . "\n";
        #echo "Succeed: " . $this->result->getSuccessCount() . "\n";
        #echo "Error: " . ($this->result->getErrorCount() > 0 ? $this->result->getErrorCount() : 0);
    }

    public function testImportSuccess()
    {
        $this->assertEquals(3, $this->result->getSuccessCount());
        $this->assertEquals(10, $this->result->getErrorCount());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->object = null;
        $this->result = null;
    }
}
