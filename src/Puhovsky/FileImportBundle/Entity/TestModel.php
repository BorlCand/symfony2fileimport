<?php

namespace Puhovsky\FileImportBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Puhovsky\ValidatorsBundle\Validator\Constraints as PAssert;

/**
 * TestModel
 */
class TestModel
{

    /**
     * @var string
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     */
    private $stringUnique;

    /**
     * @var string
     *
     * @Assert\Collection\Optional()
     * @Assert\Length(max = 10)
     */
    private $stringMax10;

    /**
     * @var double
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Type(
     *     type="numeric",
     *     message="Only positive scalars between 0 and 1000 are allowed"
     * )
     * @Assert\GreaterThanOrEqual(0.00)
     * @Assert\LessThanOrEqual(20.00)
     * @Assert\Expression(
     *     "value >= 10 or this.getIntegerPositiveString() >= 10",
     *     message="If value less than 10, integerPositiveString must be more than 10"
     * )
     */
    private $doublePositiveNumericBet0n20Expression;

    /**
     * @var integer
     *
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\GreaterThanOrEqual(0)
     * @PAssert\StringInteger()
     */
    private $integerPositiveString;

    public function __construct($item = [])
    {
        foreach ($item as $index => $value) {
            if (property_exists($this, $index)){
                $this->{$index} = $value;
            }
        }
    }

    /**
     * @return string
     */
    public function getStringMax10(): string
    {
        return $this->stringMax10;
    }

    /**
     * @param string $stringMax10
     */
    public function setStringMax10(string $stringMax10)
    {
        $this->stringMax10 = $stringMax10;
    }

    /**
     * @return string
     */
    public function getDoublePositiveNumericBet0n20Expression(): string
    {
        return $this->doublePositiveNumericBet0n20Expression;
    }

    /**
     * @param string $doublePositiveNumericBet0n20Expression
     */
    public function setDoublePositiveNumericBet0n20Expression(string $doublePositiveNumericBet0n20Expression)
    {
        $this->doublePositiveNumericBet0n20Expression = $doublePositiveNumericBet0n20Expression;
    }

    /**
     * @return string
     */
    public function getIntegerPositiveString(): string
    {
        return $this->integerPositiveString;
    }

    /**
     * @param int $integerPositiveString
     */
    public function setIntegerPositiveString(string $integerPositiveString)
    {
        $this->integerPositiveString = $integerPositiveString;
    }

    /**
     * @return string
     */
    public function getStringUnique(): string
    {
        return $this->stringUnique;
    }

    /**
     * @param string $stringUnique
     */
    public function setStringUnique(string $stringUnique)
    {
        $this->stringUnique = $stringUnique;
    }

}
