<?php

namespace Puhovsky\FileImportBundle\Importers;

use Port\Csv\CsvReader;
use Port\Exception\UnexpectedValueException;
use Port\Steps\Step;
use Port\Steps\StepAggregator as Workflow;

use Puhovsky\FileImportBundle\Port\Writers\DoctrineWriter;
use Puhovsky\FileImportBundle\Port\Writers\TableWriter;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CsvImporter
 * @package Puhovsky\FileImportBundle\Importers
 */
class CsvImporter
{
    /**
     * @var ContainerInterface|null
     */
    protected $container;

    /**
     * @var Workflow
     */
    protected $workflow;

    /**
     * @var Step[]
     */
    protected $steps = [];

    /**
     * @var array
     */
    protected $defaults = [
        "test" => false,
        "update" => false,
        "truncate" => false,
        "batch" => 1000,
        "delimiter" => ',',
        "enclosure" => '"',
        "escape" => '\\',
        "header" => null,
        "entity" => null,
        "index" => null,
    ];

    /**
     * @var null
     */
    protected $arguments = null;

    /**
     * Param $arguments can contain these options, which defaults to:
     * "test" => false,
     * "update" => false,
     * "truncate" => false,
     * "batch" => 1000,
     * "delimiter" => ',',
     * "enclosure" => '"',
     * "escape" => '\\',
     * "header" => null,
     * "entity" => null,
     *
     * @param array $arguments
     * @return $this
     */
    public function initialize(array $arguments){
        $this->arguments = array_merge($this->defaults, $arguments);

        return $this;
    }

    /**
     * Start import process
     *
     * @param \SplFileObject $file File to import
     * @return \Port\Result Result, containing basic info about import process and exceptions, which were thrown during it
     * @throws \Exception Thrown when you haven't initialized class using initialize() function
     */
    public function import(\SplFileObject $file){
        if (is_null($this->arguments)){
            throw new \Exception($messages = ['You haven\'t initialized arguments. Before calling import($file) you must call initialize(array $arguments)']);
        }

        $reader = new CsvReader($file, $this->arguments['delimiter'], $this->arguments['enclosure'], $this->arguments['escape']);
        $reader->setHeaderRowNumber(0);
        if (!is_null($this->arguments['header'])) {
            $reader->setColumnHeaders($this->arguments['header']);
        }

        $this->workflow = new Workflow($reader);
        $this->workflow->setSkipItemOnFailure(true);
        $this->setSteps()->setWriters();

        $result = $this->workflow->process();

        if ($reader->hasErrors()){
            //ToDo: maybe use EditableResult class to support updating errors count with values from reader
            //$result->setTotalProcessedCount($result->getTotalProcessedCount() + count($reader->getErrors()));
            //$result->setErrorCount($result->getErrorCount() + count($reader->getErrors()));

            foreach ($reader->getErrors() as $error) {
                $result->getExceptions()->attach(
                    new UnexpectedValueException(
                        sprintf(
                                "<error>=> Item can not be read:</error>\n<info>%s</info>",
                                json_encode($error)
                        )
                    )
                );
            }

        }

        return $result;
    }

    /**
     * Allows to add user-defined additional steps
     *
     * @param Step[] ...$params
     * @return $this
     */
    public function addSteps(Step ...$params){
        $this->steps = array_merge($this->steps, $params);

        return $this;
    }

    /**
     * Sets up additional steps
     *
     * @return $this
     */
    protected function setSteps(){
        foreach ($this->steps as $param) {
            $this->workflow->addStep($param);
        }

        return $this;
    }

    /**
     * Sets up writers depending on options
     *
     * @return $this
     */
    protected function setWriters(){
        if ($this->arguments['test']){
            $writer = new TableWriter(
                $this->container->get('validator'),
                new Table(
                    new ConsoleOutput($decorated = true)
                ),
                $this->arguments['entity'],
                $this->arguments['index']
            );

            $writer->setQueueSize($this->arguments['batch']);
        } else {
            $writer = new DoctrineWriter(
                $this->container->get('validator'),
                $this->container->get('doctrine.orm.default_entity_manager'),
                $this->arguments['entity'],
                $this->arguments['index']
            );

            $writer->setTruncate($this->arguments['truncate'])->setUpdate($this->arguments['update'])->setQueueSize($this->arguments['batch']);
        }

        $this->workflow->addWriter($writer);

        return $this;
    }

    public function setContainer(ContainerInterface $container){
        $this->container = $container;
    }
}
